package ejercio2;

public class Ejercicio2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Nombre\t\tapellido\temail\t\t\t\tMateria");
		System.out.println("------\t\t--------\t-----\t\t\t\t-------\n\n\n");
		System.out.println("Gabriel\t\tCasas\t\tgcasas1972@gmail.com\t\tComputadoras de Aeronaves");
		System.out.println("Mario\t\tArena\t\ttpinacciata@gmail.com\t\tComputadoras de Aeronaves");
		System.out.println("Bruno\t\tCimalando\tprofesor_cimalando@yahoo.com.ar\tOrg. y Op. Aer.");
		System.out.println("Walter\t\tStahler\t\twalterstahler@gmail.com\t\tSistema De Control De Vuelo");
	}

}
