package objetosPractica2.modelo;

public class Triangulo extends Figura {
	//atributos
	private float base;
	private float altura;

	//constructores
	public Triangulo() {}

	public Triangulo(String pNombre) {
		super(pNombre);
		}

	public Triangulo(String pNombre, float base, float altura) {
		super(pNombre);
		this.base = base;
		this.altura = altura;
		Figura.maximaSuperficie= Math.max(Figura.maximaSuperficie, calcularSuperficie());
	}

	//metodos de negocio
	@Override
	public float calcularPerimetro() {
		float hip= (float) Math.sqrt(base*base+altura*altura);
		return hip+base+altura;
	}

	@Override
	public float calcularSuperficie() {
		return (base*altura)/2;
	}

	@Override
	public String getTipo() {
		return "Triangulo";
	}

	@Override
	public String getValores() {
		StringBuffer sb= new StringBuffer("b=");
		sb.append(base);
		sb.append("a=");
		sb.append(altura);
		return sb.toString();
	}

	//getter y setter
	public float getBase() {
		return base;
	}

	public void setBase(float base) {
		this.base = base;
	}

	public float getAltura() {
		return altura;
	}

	public void setAltura(float altura) {
		this.altura = altura;
	}

	//hashCode y equals
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Float.floatToIntBits(altura);
		result = prime * result + Float.floatToIntBits(base);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof Triangulo)) {
			return false;
		}
		Triangulo other = (Triangulo) obj;
		if (Float.floatToIntBits(altura) != Float.floatToIntBits(other.altura)) {
			return false;
		}
		if (Float.floatToIntBits(base) != Float.floatToIntBits(other.base)) {
			return false;
		}
		return true;
	}

	//toString
	@Override
	public String toString() {
		return "base=" + base + ", altura=" + altura;
	}

}
