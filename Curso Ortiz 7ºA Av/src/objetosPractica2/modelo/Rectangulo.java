package objetosPractica2.modelo;

public class Rectangulo extends Figura {
	//atributos
	private float base;
	private float altura;

	//constructores
	public Rectangulo() {}

	public Rectangulo(String pNombre) {
		super(pNombre);
	}

	public Rectangulo(String pNom, float pBase, float pAltura) {
		super(pNom);
		this.base = pBase;
		this.altura = pAltura;
		Figura.maximaSuperficie= Math.max(Figura.maximaSuperficie, calcularSuperficie());
	}
	
	//getter y setter
	public float getBase() {
		return base;
	}

	public void setBase(float base) {
		this.base = base;
	}

	public float getAltura() {
		return altura;
	}

	public void setAltura(float altura) {
		this.altura = altura;
	}

	//metodos de negocio
	@Override
	public float calcularPerimetro() {
		return base*2+altura*2;
	}

	@Override
	public float calcularSuperficie() {
		return base*altura;
	}

	@Override
	public String getTipo() {
		return "Rectangulo";
	}

	@Override
	public String getValores() {
		StringBuffer bf= new StringBuffer("b=");
		bf.append(base);
		bf.append(", a=");
		bf.append(altura);
		return bf.toString();
	}
	
	//hashCode y equals
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Float.floatToIntBits(altura);
		result = prime * result + Float.floatToIntBits(base);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof Rectangulo)) {
			return false;
		}
		Rectangulo other = (Rectangulo) obj;
		if (Float.floatToIntBits(altura) != Float.floatToIntBits(other.altura)) {
			return false;
		}
		if (Float.floatToIntBits(base) != Float.floatToIntBits(other.base)) {
			return false;
		}
		return true;
	}
	
	//toString
	@Override
	public String toString() {
		return super.toString() + "\nbase=" + base + "\naltura=" + altura;
	}

	

}
