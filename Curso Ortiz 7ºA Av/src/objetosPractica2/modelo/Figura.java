package objetosPractica2.modelo;

/**
 * @author Gonzalo
 * Esta es la clase padre de las figuras
 */
public abstract class Figura implements Model{
	//atributo
	private String nombre;
	protected static float maximaSuperficie;

	//constructores
	public Figura() {
	}

	public Figura(String pNombre) {
		super();
		this.nombre = pNombre;
	}

	//getter y setter
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String pNombre) {
		this.nombre = pNombre;
	}
	
	
	//metodos abstractos
	
	/**
	 * calcula parimetro de las figuras
	 * @return valor float con el perimetro
	 */
	public abstract float calcularPerimetro();
	/**
	 * calcula superficie de las figuras
	 * @return valor float con la superficie
	 */
	public abstract float calcularSuperficie();
	
	public abstract String getTipo();
	
	/**Este metodo devuelve los valores de nuestra figura para ser mostrados en una grilla o cualquier grafico
	 * @return
	 */
	public abstract String getValores();
	
	//metodo estatico
	public static float getMaximaSuperficie(){
		return maximaSuperficie;
	}
	
	//hashCode y equals
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Figura)) {
			return false;
		}
		Figura other = (Figura) obj;
		if (nombre == null) {
			if (other.getNombre() != null) {
				return false;
			}
		} else if (!nombre.equals(other.getNombre())) {
			return false;
		}
		return true;
	}

	//toString
	@Override
	public String toString() {
		return "\nNombre=" + nombre;
	}
	
	
	

}
