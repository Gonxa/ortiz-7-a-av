package objetosPractica2.modelo.controller;

import objetosPractica2.modelo.Figura;
import objetosPractica2.modelo.exception.FiguraException;

public interface Controller {
	public void addHandler(Figura fig)throws FiguraException;
	public void leerHandler(Figura fig)throws FiguraException;
	public void modifyHandler(Figura fig)throws FiguraException;
	public void removeHandler(Figura fig)throws FiguraException;
}
