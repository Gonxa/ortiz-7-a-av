package objetosPractica2.modelo.controller.composite;

import java.util.ArrayList;
import java.util.List;

import objetosPractica2.modelo.Figura;

public abstract class ValidatorComposite {
	//atributos
	protected static Figura figura;
	
	//constructor
	public ValidatorComposite() {
	}
	
	//metodo estatico
	public static String getErrores(Figura fig){
		figura=fig;
		List<ValidatorComposite> validaciones = new ArrayList<ValidatorComposite>();
		
		validaciones.add(new FiguraNombreDobleEspacioComposite());
		validaciones.add(new FiguraNombreVacioComposite());
		//rectangulo
		validaciones.add(new FiguraRectanguloAlturaCeroNegativoComposite());
		validaciones.add(new RectanguloBaseCeroNegativoComposite());
		//circulo
		validaciones.add(new CirculoRadioCeroNegativoComposite());
		
		StringBuffer sbErrores = new StringBuffer();
		
		for (ValidatorComposite validacion: validaciones){
			if (validacion.isMe() && validacion.validar()){
				sbErrores.append(validacion.getError());
				sbErrores.append("\n");
			}
		}
		return sbErrores.toString();
	};
	
	//abstractos
	public abstract boolean isMe();
	/**
	 * Este matodo devuelve true en caso de que se de el error
	 * @return
	 */
	public abstract String getError();
	public abstract boolean validar();
	
}
