package objetosPractica2.modelo.controller.composite;

import objetosPractica2.modelo.Rectangulo;

public class FiguraRectanguloAlturaCeroNegativoComposite extends
		ValidatorComposite {

	public FiguraRectanguloAlturaCeroNegativoComposite() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean isMe() {
		return figura instanceof Rectangulo;
	}

	@Override
	public String getError() {
		return "La altura debe ser mayor a 0";
	}

	@Override
	public boolean validar() {
		Rectangulo rec= (Rectangulo)figura;
		return rec.getAltura()<=0;
	}

}
