package objetosPractica2.modelo.controller.composite;

import objetosPractica2.modelo.Circulo;

public class CirculoRadioCeroNegativoComposite extends ValidatorComposite {

	public CirculoRadioCeroNegativoComposite() {
	}

	@Override
	public boolean isMe() {
		return figura instanceof Circulo;
	}

	@Override
	public String getError() {
		return "El Radio debe ser mayor que cero";
	}

	@Override
	public boolean validar() {
		Circulo cir= (Circulo)figura;
		return cir.getRadio()<=0;
	}

}
