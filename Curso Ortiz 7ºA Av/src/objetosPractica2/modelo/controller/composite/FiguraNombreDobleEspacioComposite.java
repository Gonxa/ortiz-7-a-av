package objetosPractica2.modelo.controller.composite;

public class FiguraNombreDobleEspacioComposite extends ValidatorComposite {

	public FiguraNombreDobleEspacioComposite() {
	}

	@Override
	public boolean isMe() {
		return true;
	}

	@Override
	public String getError() {
		return "El nombre no puede tener dos espacios";
	}

	@Override
	public boolean validar() {
		return figura.getNombre().contains("  ");
	}

}
