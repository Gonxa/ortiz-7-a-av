package objetosPractica2.modelo.controller.composite.test;

import static org.junit.Assert.*;

import objetosPractica2.modelo.Circulo;
import objetosPractica2.modelo.Figura;
import objetosPractica2.modelo.Rectangulo;
import objetosPractica2.modelo.controller.composite.ValidatorComposite;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ValidatorCompositeTest {
	Figura figTest1;
	Figura figTest2;
	Figura figTest3;
	Figura figTest4;
	Figura figTest5;
	Figura figTest6;
	Figura figTest7;

	@Before
	public void setUp() throws Exception {
		figTest1= new Rectangulo("Rec  1", 0, -4);
		figTest2= new Rectangulo("Rec  1", 1, 1);
		figTest3= new Rectangulo("Rec1", 0, 4);
		figTest4= new Rectangulo("Rec1", 1, -4);
		figTest5= new Circulo("", -12);
		figTest6= new Circulo("cir", -12);
		figTest7= new Circulo("", 12);
	}

	@After
	public void tearDown() throws Exception {
		figTest1=null;
		figTest2=null;
	}

	@Test
	public void testGetErroresDobleEspacioYRectangulo() {
		assertEquals("El nombre no puede tener dos espacios\nLa altura debe ser mayor a 0\nLa base debe ser mayor a 0\n", ValidatorComposite.getErrores(figTest1));
	}
	
	@Test
	public void testGetErroresDobleEspacio() {
		assertEquals("El nombre no puede tener dos espacios\n", ValidatorComposite.getErrores(figTest2));
	}
	
	@Test
	public void testGetErroresRectanguloBase() {
		assertEquals("La base debe ser mayor a 0\n", ValidatorComposite.getErrores(figTest3));
	}
	
	@Test
	public void testGetErroresRectanguloAltura() {
		assertEquals("La altura debe ser mayor a 0\n", ValidatorComposite.getErrores(figTest4));
	}
	
	@Test
	public void testGetErroresNombreVacioYCirculo() {
		assertEquals("El nombre est� vacio\nEl Radio debe ser mayor que cero\n", ValidatorComposite.getErrores(figTest5));
		
	}
	
	@Test
	public void testGetErroresNombreVacio() {
		assertEquals("El Radio debe ser mayor que cero\n", ValidatorComposite.getErrores(figTest6));
		
	}
	
	@Test
	public void testGetErroresCirculoRadio() {
		assertEquals("El nombre est� vacio\n", ValidatorComposite.getErrores(figTest7));
		
	}
}
