package objetosPractica2.modelo.controller;

import objetosPractica2.modelo.Figura;
import objetosPractica2.modelo.controller.composite.ValidatorComposite;
import objetosPractica2.modelo.exception.FiguraException;

public class FiguraController implements Controller {

	public FiguraController() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addHandler(Figura fig) throws FiguraException {
		String StrErroes= ValidatorComposite.getErrores(fig);
		if (!StrErroes.isEmpty()){
			throw new FiguraException(StrErroes);
		}
	}

	@Override
	public void leerHandler(Figura fig) throws FiguraException {
		// TODO Auto-generated method stub

	}

	@Override
	public void modifyHandler(Figura fig) throws FiguraException {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeHandler(Figura fig) throws FiguraException {
		// TODO Auto-generated method stub

	}

}
