package objetosPractica2.modelo;

public class PoligonoRegular extends Figura {
	//atributos
	private float lado;
	private float CantidadDeLados;
	private float apotema;

	//constructores
	public PoligonoRegular() {}

	public PoligonoRegular(String pNombre) {
		super(pNombre);
	}

	public PoligonoRegular(String pNom, float lado, float apo, float cant) {
		super(pNom);
		this.lado = lado;
		this.apotema = apo;
		this.CantidadDeLados = cant;
		Figura.maximaSuperficie= Math.max(Figura.maximaSuperficie, calcularSuperficie());
	}

	//metodos de negocio
	@Override
	public float calcularPerimetro() {
		return lado*CantidadDeLados;
	}

	@Override
	public float calcularSuperficie() {
		return (lado*CantidadDeLados*apotema)/2;
	}

	@Override
	public String getTipo() {
		return "Poligono";
	}

	@Override
	public String getValores() {
		StringBuffer sb= new StringBuffer("t=");
		sb.append(lado);
		sb.append(", a=");
		sb.append(apotema);
		sb.append(", c=");
		sb.append(CantidadDeLados);
		return sb.toString();
	}

	//getter y setter
	public float getLado() {
		return lado;
	}

	public void setLado(float lado) {
		this.lado = lado;
	}

	public float getCantidadDeLados() {
		return CantidadDeLados;
	}

	public void setCantidadDeLados(float cant) {
		this.CantidadDeLados = cant;
	}

	public float getApotema() {
		return apotema;
	}

	public void setApotema(float apo) {
		this.apotema = apo;
	}

	//hashCode y equals
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Float.floatToIntBits(apotema);
		result = prime * result + Float.floatToIntBits(CantidadDeLados);
		result = prime * result + Float.floatToIntBits(lado);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof PoligonoRegular)) {
			return false;
		}
		PoligonoRegular other = (PoligonoRegular) obj;
		if (Float.floatToIntBits(apotema) != Float.floatToIntBits(other.apotema)) {
			return false;
		}
		if (Float.floatToIntBits(CantidadDeLados) != Float.floatToIntBits(other.CantidadDeLados)) {
			return false;
		}
		if (Float.floatToIntBits(lado) != Float.floatToIntBits(other.lado)) {
			return false;
		}
		return true;
	}

	//toString
	@Override
	public String toString() {
		return "lado=" + lado + ", cant=" + CantidadDeLados + ", apo=" + apotema;
	}
}
