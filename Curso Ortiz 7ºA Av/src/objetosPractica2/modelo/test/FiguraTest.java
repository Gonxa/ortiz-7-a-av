package objetosPractica2.modelo.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import junit.framework.Assert;

import objetosPractica2.modelo.Circulo;
import objetosPractica2.modelo.Cuadrado;
import objetosPractica2.modelo.Figura;
import objetosPractica2.modelo.PoligonoRegular;
import objetosPractica2.modelo.Rectangulo;
import objetosPractica2.modelo.Triangulo;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FiguraTest {
	
	List<Figura> lstFigura= new ArrayList<Figura>();
	Set<Figura> setFigura= new HashSet<Figura>();
	
	Cuadrado cua;
	Circulo cir;
	Rectangulo rec;
	Triangulo tri;
	PoligonoRegular pol;
	
	@Before
	public void setUp() throws Exception {
		cua= new Cuadrado("CuadradoTest", 10);
		cir= new Circulo("CirculoTest", 10);
		rec= new Rectangulo("RectanguloTest", 10, 20);
		tri= new Triangulo("TrianguloTest", 10, 20);
		pol= new PoligonoRegular("PoligonoTest", 10, 3, 5);
		
		lstFigura.add(cua);
		lstFigura.add(cir);
		lstFigura.add(rec);
		lstFigura.add(tri);
		lstFigura.add(pol);
		
		setFigura.add(cua);
		setFigura.add(cir);
		setFigura.add(rec);
		setFigura.add(tri);
		setFigura.add(pol);
	}

	@After
	public void tearDown() throws Exception {
		cua= null;
		cir= null;
		rec= null;
		lstFigura=null;
		setFigura=null;
		
	}

	@Test
	public void testGetNombreCuad() {
		Assert.assertEquals("CuadradoTest", cua.getNombre());
	}
	
	@Test
	public void testGetLadoCuad() {
		Assert.assertEquals(10f, cua.getLado());
	}
	
	@Test
	public void testGetNombreCir() {
		Assert.assertEquals("CirculoTest", cir.getNombre());
	}
	
	@Test
	public void testGetRadioCir() {
		Assert.assertEquals(10f, cir.getRadio());
	}
	
	@Test
	public void testGetNombreRec() {
		Assert.assertEquals("RectanguloTest", rec.getNombre());
	}
	
	@Test
	public void testGetBaseRec() {
		Assert.assertEquals(10f, rec.getBase());
	}
	
	@Test
	public void testGetAlturaRec() {
		Assert.assertEquals(20f, rec.getAltura());
	}
	
	@Test
	public void testGetNombreTri() {
		Assert.assertEquals("TrianguloTest", tri.getNombre());
	}
	
	@Test
	public void testGetBaseTri() {
		Assert.assertEquals(10f, tri.getBase());
	}
	
	@Test
	public void testGetAlturaTri() {
		Assert.assertEquals(20f, tri.getAltura());
	}
	
	@Test
	public void testGetNombrePol() {
		Assert.assertEquals("PoligonoTest", pol.getNombre());
	}
	
	@Test
	public void testGetLadoPol() {
		Assert.assertEquals(10f, pol.getLado());
	}
	
	@Test
	public void testGetApotema() {
		Assert.assertEquals(3f, pol.getApotema());
	}
	
	@Test
	public void testGetCantidadDeLados() {
		Assert.assertEquals(5f, pol.getCantidadDeLados());
	}

	@Test
	public void testCalcularPerimetroCuad() {
		Assert.assertEquals(40.0f, cua.calcularPerimetro());
	}

	@Test
	public void testCalcularSuperficieCuad() {
		Assert.assertEquals(100.0f, cua.calcularSuperficie());
	}
	
	@Test
	public void testCalcularPerimetroCir(){
		Assert.assertEquals(62.832, cir.calcularPerimetro(), 0.001);
	}
	
	@Test
	public void testCalcularSuperficieCir(){
		Assert.assertEquals(Math.PI*100, cir.calcularSuperficie(), 0.001);
	}
	
	@Test
	public void testCalcularPerimetroRec(){
		Assert.assertEquals(60.0f, rec.calcularPerimetro());
	}
	
	@Test
	public void testCalcularSuperficieRec(){
		Assert.assertEquals(200.0f, rec.calcularSuperficie());
	}
	
	@Test
	public void testCalcularPerimetroTri(){
		Assert.assertEquals(52.36f, tri.calcularPerimetro(), 0.01);
	}
	
	@Test
	public void testCalcularSuperficieTri(){
		Assert.assertEquals(100f, tri.calcularSuperficie());
	}
	
	@Test
	public void testCalcularPerimetroPol(){
		Assert.assertEquals(50f, pol.calcularPerimetro());
	}
	
	@Test
	public void testCalcularSuperficiePol(){
		Assert.assertEquals(75f, pol.calcularSuperficie());
	}
	
	@Test
	public void testListaContieneCuadrado(){
		Assert.assertTrue(lstFigura.contains(cua));
	}
	
	@Test
	public void testListaContieneCirculo(){
		Assert.assertTrue(lstFigura.contains(cir));
	}
	
	@Test
	public void testListaContieneRectangulo(){
		Assert.assertTrue(lstFigura.contains(rec));
	}
	
	@Test
	public void testListaContieneTriangulo(){
		Assert.assertTrue(lstFigura.contains(tri));
	}
	
	@Test
	public void testListaContienePoligono(){
		Assert.assertTrue(lstFigura.contains(pol));
	}
	
	@Test
	public void testSetContieneCuadrado(){
		Assert.assertTrue(setFigura.contains(cua));
	}
	
	@Test
	public void testSetContieneCirculo(){
		Assert.assertTrue(setFigura.contains(cir));
	}
	
	@Test
	public void testSetContieneRectangulo(){
		Assert.assertTrue(setFigura.contains(rec));
	}
	
	@Test
	public void testSetContieneTriangulo(){
		Assert.assertTrue(setFigura.contains(tri));
	}
	
	@Test
	public void testSetContienePoligono(){
		Assert.assertTrue(setFigura.contains(pol));
	}
	
	@Test
	public void testAgregarCuaAAlista(){
		lstFigura.add(new Cuadrado("CuadradoTest", 10));
		assertEquals(6, lstFigura.size());
	}
	
	@Test
	public void testAgregarCirAAlista(){
		lstFigura.add(new Circulo("CirculoTest", 10));
		assertEquals(6, lstFigura.size());
	}
	
	@Test
	public void testAgregarRecAAlista(){
		lstFigura.add(new Rectangulo("RectanguloTest", 10, 20));
		assertEquals(6, lstFigura.size());
	}
	
	@Test
	public void testAgregarTriAAlista(){
		lstFigura.add(new Triangulo("TrianguloTest", 10, 20));
		assertEquals(6, lstFigura.size());
	}
	
	@Test
	public void testAgregarPolAAlista(){
		lstFigura.add(new PoligonoRegular("PoligonoTest", 10, 3, 5));
		assertEquals(6, lstFigura.size());
	}
	
	@Test
	public void testAgregarCuaASet(){
		setFigura.add(new Cuadrado("CuadradoTest", 10));
		assertEquals(5, setFigura.size());
	}
	
	@Test
	public void testAgregarCirASet(){
		setFigura.add(new Circulo("CirculoTest", 10));
		assertEquals(5, setFigura.size());
	}
	
	@Test
	public void testAgregarRecASet(){
		setFigura.add(new Rectangulo("RectanguloTest", 10, 20));
		assertEquals(5, setFigura.size());
	}
	
	@Test
	public void testAgregarTriASet(){
		setFigura.add(new Triangulo("TrianguloTest", 10, 20));
		assertEquals(5, setFigura.size());
	}
	
	@Test
	public void testAgregarPolASet(){
		setFigura.add(new PoligonoRegular("PoligonoTest", 10, 3, 5));
		assertEquals(5, setFigura.size());
	}

}
