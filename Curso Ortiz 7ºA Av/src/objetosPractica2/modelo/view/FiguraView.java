package objetosPractica2.modelo.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;

import objetosPractica2.modelo.Circulo;
import objetosPractica2.modelo.Cuadrado;
import objetosPractica2.modelo.Figura;
import objetosPractica2.modelo.PoligonoRegular;
import objetosPractica2.modelo.Rectangulo;
import objetosPractica2.modelo.Triangulo;
import objetosPractica2.modelo.controller.FiguraController;
import objetosPractica2.modelo.exception.FiguraException;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.ListSelectionModel;
import java.awt.Color;

public class FiguraView {

	private JFrame frame;
	private JTextField txtNombre;
	private JTextField txtValor1;
	private JTable table;
	
	private List<Figura> figuras= new ArrayList<Figura>();
	private String arrayFiguras[][];
	private Figura FiguraAmodificarEliminar;
	private JTextField txtValor2;
	private JTextField txtValor3;
	private JLabel lblValor2;
	private JLabel lblValor3;
	private JLabel lblValor1;
	private JLabel lblNombre;
	private JRadioButton rdbtnCirculo;
	private JRadioButton rdbtnCuadrado;
	private JRadioButton rdbtnRectangulo;
	private JRadioButton rdbtnTriangulo;
	private JRadioButton rdbtnPoligono;
	private JLabel lblMaximaSuperficie;
	private JLabel lblMax;
	
	//agregrar controller
	FiguraController figuraController = new FiguraController();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FiguraView window = new FiguraView();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public FiguraView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 804, 499);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		lblNombre = new JLabel("Nombre");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNombre.setBounds(116, 30, 88, 25);
		frame.getContentPane().add(lblNombre);
		
		lblValor1 = new JLabel("Radio");
		lblValor1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblValor1.setBounds(116, 86, 101, 25);
		frame.getContentPane().add(lblValor1);
		
		txtNombre = new JTextField();
		txtNombre.setFont(new Font("Tahoma", Font.PLAIN, 20));
		txtNombre.setBounds(314, 27, 176, 31);
		frame.getContentPane().add(txtNombre);
		txtNombre.setColumns(10);
		
		rdbtnCirculo = new JRadioButton("Circulo");
		rdbtnCirculo.setEnabled(false);
		rdbtnCirculo.setSelected(true);
		rdbtnCirculo.setBounds(540, 35, 109, 23);
		frame.getContentPane().add(rdbtnCirculo);
		
		rdbtnCuadrado = new JRadioButton("Cuadrado");
		rdbtnCuadrado.setBounds(540, 61, 109, 23);
		frame.getContentPane().add(rdbtnCuadrado);
		
		rdbtnRectangulo = new JRadioButton("Rectangulo");
		rdbtnRectangulo.setBounds(540, 87, 109, 23);
		frame.getContentPane().add(rdbtnRectangulo);
		
		rdbtnPoligono = new JRadioButton("Poligono");
		rdbtnPoligono.setBounds(540, 139, 109, 23);
		frame.getContentPane().add(rdbtnPoligono);
		
		rdbtnTriangulo = new JRadioButton("Triangulo");
		rdbtnTriangulo.setBounds(540, 113, 109, 23);
		frame.getContentPane().add(rdbtnTriangulo);
		
		rdbtnCuadrado.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				setCuadrado();
			}
		});
		rdbtnCirculo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				setCirculo();
			}
		});
		rdbtnRectangulo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				setRectangulo();
			}
		});
		rdbtnTriangulo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {				
				setTriangulo();
			}
		});
		rdbtnPoligono.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				setPoligono();
			}
		});
		
		txtValor1 = new JTextField();
		txtValor1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		txtValor1.setColumns(10);
		txtValor1.setBounds(314, 80, 176, 31);
		frame.getContentPane().add(txtValor1);
		
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnAgregar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
					if (rdbtnCuadrado.isSelected()){
						Cuadrado cua= new Cuadrado(txtNombre.getText(), Float.parseFloat(txtValor1.getText()));
						try {
							figuraController.addHandler(cua);
							figuras.add(cua);
						} catch (FiguraException e1) {
							JOptionPane.showMessageDialog(null, e1.getMessage());
							e1.printStackTrace();
						}
					}
					else if (rdbtnCirculo.isSelected()){
						Circulo cir= new Circulo(txtNombre.getText(), Float.parseFloat(txtValor1.getText()));
						try {
							figuraController.addHandler(cir);
							figuras.add(cir);
						} catch (FiguraException e1) {
							JOptionPane.showMessageDialog(null, e1.getMessage());
							e1.printStackTrace();
						}
					}
					else if (rdbtnRectangulo.isSelected()){
						Rectangulo rec= new Rectangulo(txtNombre.getText(), Float.parseFloat(txtValor1.getText()), Float.parseFloat(txtValor2.getText()));
						try {
							figuraController.addHandler(rec);
							figuras.add(rec);
						} catch (FiguraException e1) {
							JOptionPane.showMessageDialog(null, e1.getMessage());
							e1.printStackTrace();
						}
					}
					else if (rdbtnTriangulo.isSelected()){
						Triangulo tri= new Triangulo(txtNombre.getText(), Float.parseFloat(txtValor1.getText()), Float.parseFloat(txtValor2.getText()));
						try {
							figuraController.addHandler(tri);
							figuras.add(tri);
						} catch (FiguraException e1) {
							JOptionPane.showMessageDialog(null, e1.getMessage());
							e1.printStackTrace();
						}
					}
					else if (rdbtnPoligono.isSelected()){
						PoligonoRegular pol= new PoligonoRegular(txtNombre.getText(), Float.parseFloat(txtValor1.getText()), Float.parseFloat(txtValor2.getText()), Float.parseFloat(txtValor3.getText()));
						try {
							figuraController.addHandler(pol);
							figuras.add(pol);
						} catch (FiguraException e1) {
							JOptionPane.showMessageDialog(null, e1.getMessage());
							e1.printStackTrace();
						}
					}
					
				llenarGrilla(figuras);
				limpiarCampos();
			}
		});
		btnAgregar.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnAgregar.setBounds(33, 289, 120, 33);
		frame.getContentPane().add(btnAgregar);
		
		JButton btnModificar = new JButton("Modificar");
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnModificar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (txtNombre.getText().isEmpty() || txtValor1.getText().isEmpty()){
					JOptionPane.showMessageDialog(null, "Llene las casillas");
					return;
				}
				if (FiguraAmodificarEliminar==null){
					JOptionPane.showMessageDialog(null, "Seleccione una casilla");
				}else{
					if (rdbtnCuadrado.isSelected()){
						Cuadrado cua= new Cuadrado(txtNombre.getText(), Float.parseFloat(txtValor1.getText()));
						try {
							figuraController.addHandler(cua);
							figuras.set(table.getSelectedRow(), cua);
						} catch (FiguraException e1) {
							JOptionPane.showMessageDialog(null, e1.getMessage());
							e1.printStackTrace();
						}
					}else if (rdbtnCirculo.isSelected()){
						Circulo cir= new Circulo(txtNombre.getText(), Float.parseFloat(txtValor1.getText()));
						try {
							figuraController.addHandler(cir);
							figuras.set(table.getSelectedRow(), cir);
						} catch (FiguraException e1) {
							JOptionPane.showMessageDialog(null, e1.getMessage());
							e1.printStackTrace();
						}
					}else if (rdbtnRectangulo.isSelected()){
						Rectangulo rec= new Rectangulo(txtNombre.getText(), Float.parseFloat(txtValor1.getText()), Float.parseFloat(txtValor2.getText()));
						try {
							figuraController.addHandler(rec);
							figuras.set(table.getSelectedRow(), rec);
						} catch (FiguraException e1) {
							JOptionPane.showMessageDialog(null, e1.getMessage());
							e1.printStackTrace();
						}
					}else if (rdbtnTriangulo.isSelected()){
						Triangulo tri= new Triangulo(txtNombre.getText(), Float.parseFloat(txtValor1.getText()), Float.parseFloat(txtValor2.getText()));
						try {
							figuraController.addHandler(tri);
							figuras.set(table.getSelectedRow(), tri);
						} catch (FiguraException e1) {
							JOptionPane.showMessageDialog(null, e1.getMessage());
							e1.printStackTrace();
						}
					}else if (rdbtnPoligono.isSelected()){
						PoligonoRegular pol= new PoligonoRegular(txtNombre.getText(), Float.parseFloat(txtValor1.getText()), Float.parseFloat(txtValor2.getText()), Float.parseFloat(txtValor3.getText()));
						try {
							figuraController.addHandler(pol);
							figuras.set(table.getSelectedRow(), pol);
						} catch (FiguraException e1) {
							JOptionPane.showMessageDialog(null, e1.getMessage());
							e1.printStackTrace();
						}
					}
				}
				FiguraAmodificarEliminar=null;
				llenarGrilla(figuras);
			}
		});
		btnModificar.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnModificar.setBounds(176, 289, 129, 33);
		frame.getContentPane().add(btnModificar);
		
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (FiguraAmodificarEliminar==null){
					JOptionPane.showMessageDialog(null, "Seleccione una casilla");
				}else{
					figuras.remove(((table.getSelectedRow())));
					llenarGrilla(figuras);
					FiguraAmodificarEliminar=null;
				}
			}
		});
		btnEliminar.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnEliminar.setBounds(495, 289, 113, 33);
		frame.getContentPane().add(btnEliminar);
		
		JButton btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				figuras.removeAll(figuras);
				llenarGrilla(figuras);
			}
		});
		btnLimpiar.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnLimpiar.setBounds(638, 289, 113, 33);
		frame.getContentPane().add(btnLimpiar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(33, 333, 718, 116);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				FiguraAmodificarEliminar=figuras.get(table.getSelectedRow());
			}
		});
		scrollPane.setViewportView(table);
		
		JButton btnNewButton = new JButton("Rellenar");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnNewButton.setSelectedIcon(null);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			if (FiguraAmodificarEliminar==null){
				JOptionPane.showMessageDialog(null, "Seleccione una casilla");
			}else{
					if(FiguraAmodificarEliminar instanceof Cuadrado){
						setCuadrado();
						asignarValores((Cuadrado)FiguraAmodificarEliminar);
					}else if(FiguraAmodificarEliminar instanceof Circulo){
						setCirculo();
						asignarValores((Circulo)FiguraAmodificarEliminar);
					}else if(FiguraAmodificarEliminar instanceof Rectangulo){
						setRectangulo();
						asignarValores((Rectangulo)FiguraAmodificarEliminar);
					}else if(FiguraAmodificarEliminar instanceof Triangulo){
						setTriangulo();
						asignarValores((Triangulo)FiguraAmodificarEliminar);
					}else if(FiguraAmodificarEliminar instanceof PoligonoRegular){
						setPoligono();
						asignarValores((PoligonoRegular)FiguraAmodificarEliminar);
					FiguraAmodificarEliminar=null;
					}
				}
			}
		});
		btnNewButton.setBounds(331, 289, 137, 33);
		frame.getContentPane().add(btnNewButton);
		
		txtValor2 = new JTextField();
		txtValor2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		txtValor2.setColumns(10);
		txtValor2.setBounds(314, 133, 176, 31);
		frame.getContentPane().add(txtValor2);
		txtValor2.setVisible(false);
		
		txtValor3 = new JTextField();
		txtValor3.setFont(new Font("Tahoma", Font.PLAIN, 20));
		txtValor3.setColumns(10);
		txtValor3.setBounds(314, 179, 176, 31);
		frame.getContentPane().add(txtValor3);
		txtValor3.setVisible(false);
		
		lblValor2 = new JLabel("Lado/Radio");
		lblValor2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblValor2.setBounds(116, 139, 101, 25);
		frame.getContentPane().add(lblValor2);
		lblValor2.setVisible(false);
		
		lblValor3 = new JLabel("Lado/Radio");
		lblValor3.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblValor3.setBounds(116, 185, 101, 25);
		frame.getContentPane().add(lblValor3);
		
		lblMaximaSuperficie = new JLabel("Maxima Superficie= ");
		lblMaximaSuperficie.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblMaximaSuperficie.setBounds(273, 237, 183, 42);
		frame.getContentPane().add(lblMaximaSuperficie);
		
		lblMax = new JLabel("Max");
		lblMax.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblMax.setBounds(453, 235, 146, 47);
		frame.getContentPane().add(lblMax);
		lblValor3.setVisible(false);
		
		tablaInicio();
		
	}
	
	private void llenarGrilla(List<Figura> pFigura){
		int fila =0 ;
		arrayFiguras = new String[pFigura.size()][5];
		DecimalFormat df =new DecimalFormat("#.##");
		
		for (Figura figura : pFigura) {
			for(int col=0;col<5;col++){
				switch (col) {
				case 0:
					arrayFiguras[fila][col] = figura.getNombre();				
					break;
				case 1:
					arrayFiguras[fila][col] = figura.getTipo();						
					break;
				case 2:
					arrayFiguras[fila][col] = figura.getValores();
					break;
				case 3:
					arrayFiguras[fila][col] = df.format(figura.calcularPerimetro());				
					break;
				case 4:
					arrayFiguras[fila][col] = df.format(figura.calcularSuperficie());					
					break;
				default:
					break;
				}				
			}
			fila++;
		} 
		table.setModel(new DefaultTableModel(
				arrayFiguras,
				new String[] {
					"Nombre", "Tipo", "Valores", "Perimetro", "Superficie", 
				}
			));
		lblMax.setText(Float.toString(Figura.getMaximaSuperficie()));
	}

	
	private void asignarValores(Cuadrado cuad){
		txtNombre.setText(cuad.getNombre());
		txtValor1.setText(Float.toString(cuad.getLado()));
	}
	
	private void asignarValores(Circulo cir){
		txtNombre.setText(cir.getNombre());
		txtValor1.setText(Float.toString(cir.getRadio()));
	}
	
	private void asignarValores(Rectangulo rec){
		txtNombre.setText(rec.getNombre());
		txtValor1.setText(Float.toString(rec.getBase()));
		txtValor2.setText(Float.toString(rec.getAltura()));
	}
	
	private void asignarValores(Triangulo tri){
		txtNombre.setText(tri.getNombre());
		txtValor1.setText(Float.toString(tri.getBase()));
		txtValor2.setText(Float.toString(tri.getAltura()));
	}
	
	private void asignarValores(PoligonoRegular tri){
		txtNombre.setText(tri.getNombre());
		txtValor1.setText(Float.toString(tri.getLado()));
		txtValor2.setText(Float.toString(tri.getApotema()));
		txtValor3.setText(Float.toString(tri.getCantidadDeLados()));
	}
	
	private void limpiarCampos(){
		txtNombre.setText("");
		txtValor1.setText("");
		txtValor2.setText("");
		txtValor3.setText("");
	}
	
	private void tablaInicio(){
		figuras.add(new Cuadrado("Cuad1", 20));
		figuras.add(new Circulo("Cir1", 10));
		figuras.add(new Rectangulo("Rec1", 10, 20));
		figuras.add(new Triangulo("Tri1", 10, 20));
		figuras.add(new PoligonoRegular("Pol1", 10, 5, 3));
		llenarGrilla(figuras);
	}
	
	private void setCuadrado(){
		rdbtnCuadrado.setSelected(true);
		rdbtnCirculo.setSelected(false);
		rdbtnRectangulo.setSelected(false);
		rdbtnTriangulo.setSelected(false);
		rdbtnPoligono.setSelected(false);
		
		rdbtnCuadrado.setEnabled(false);
		rdbtnCirculo.setEnabled(true);
		rdbtnRectangulo.setEnabled(true);
		rdbtnTriangulo.setEnabled(true);
		rdbtnPoligono.setEnabled(true);
		
		lblValor1.setText("Lado");
		
		lblValor2.setVisible(false);
		txtValor2.setVisible(false);
		
		lblValor3.setVisible(false);
		txtValor3.setVisible(false);
	}
	
	private void setCirculo(){
		rdbtnCuadrado.setSelected(false);
		rdbtnCirculo.setSelected(true);
		rdbtnRectangulo.setSelected(false);
		rdbtnTriangulo.setSelected(false);
		rdbtnPoligono.setSelected(false);
		
		rdbtnCuadrado.setEnabled(true);
		rdbtnCirculo.setEnabled(false);
		rdbtnRectangulo.setEnabled(true);
		rdbtnTriangulo.setEnabled(true);
		rdbtnPoligono.setEnabled(true);
		
		lblValor1.setText("Radio");
		
		lblValor2.setVisible(false);
		txtValor2.setVisible(false);
		
		lblValor3.setVisible(false);
		txtValor3.setVisible(false);
	}
	
	private void setRectangulo(){
		rdbtnCuadrado.setSelected(false);
		rdbtnCirculo.setSelected(false);
		rdbtnRectangulo.setSelected(true);
		rdbtnTriangulo.setSelected(false);
		rdbtnPoligono.setSelected(false);
		
		rdbtnCuadrado.setEnabled(true);
		rdbtnCirculo.setEnabled(true);
		rdbtnRectangulo.setEnabled(false);
		rdbtnTriangulo.setEnabled(true);
		rdbtnPoligono.setEnabled(true);
		
		lblValor1.setText("Base");
		
		lblValor2.setVisible(true);
		lblValor2.setText("Altura");
		txtValor2.setVisible(true);
		
		lblValor3.setVisible(false);
		txtValor3.setVisible(false);
	}
	
	private void setTriangulo(){
		rdbtnCuadrado.setSelected(false);
		rdbtnCirculo.setSelected(false);
		rdbtnRectangulo.setSelected(false);
		rdbtnTriangulo.setSelected(true);
		rdbtnPoligono.setSelected(false);
		
		rdbtnCuadrado.setEnabled(true);
		rdbtnCirculo.setEnabled(true);
		rdbtnRectangulo.setEnabled(true);
		rdbtnTriangulo.setEnabled(false);
		rdbtnPoligono.setEnabled(true);
		
		lblValor1.setText("Base");
		
		lblValor2.setVisible(true);
		lblValor2.setText("Altura");
		txtValor2.setVisible(true);
		
		lblValor3.setVisible(false);
		txtValor3.setVisible(false);
	}
	
	private void setPoligono(){
		rdbtnCuadrado.setSelected(false);
		rdbtnCirculo.setSelected(false);
		rdbtnRectangulo.setSelected(false);
		rdbtnTriangulo.setSelected(false);
		rdbtnPoligono.setSelected(true);
		
		rdbtnCuadrado.setEnabled(true);
		rdbtnCirculo.setEnabled(true);
		rdbtnRectangulo.setEnabled(true);
		rdbtnTriangulo.setEnabled(true);
		rdbtnPoligono.setEnabled(false);
		
		lblValor1.setText("Tama�o");
		
		lblValor2.setVisible(true);
		lblValor2.setText("Apotema");
		txtValor2.setVisible(true);
		
		lblValor3.setVisible(true);
		lblValor3.setText("Cantidad");
		txtValor3.setVisible(true);
	}
}
