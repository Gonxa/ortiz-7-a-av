package ejerciciosConPantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font;

public class Ejer7 {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JLabel lblPrimerNumero;
	private JLabel lblSegundoNumero;
	private JLabel lblTercerNumero;
	private JLabel lblElMayorNumero;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejer7 window = new Ejer7();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejer7() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 515, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(41, 54, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(41, 113, 86, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(41, 181, 86, 20);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		final JLabel label = new JLabel("");
		label.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label.setBounds(300, 113, 86, 29);
		frame.getContentPane().add(label);
		
		JButton button = new JButton("");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int a= Integer.parseInt(textField.getText());
				int b= Integer.parseInt(textField_1.getText());
				int c= Integer.parseInt(textField_2.getText());
				int[] nums = {a, b, c};
				int mayor= a;
				for (int i=0; i<3; i++){
					if (nums[i]>mayor){mayor=nums[i];}
				}
				label.setText(Integer.toString(mayor));
			}
		});
		button.setBounds(166, 112, 89, 23);
		frame.getContentPane().add(button);
		
		lblPrimerNumero = new JLabel("Primer numero");
		lblPrimerNumero.setBounds(41, 29, 86, 14);
		frame.getContentPane().add(lblPrimerNumero);
		
		lblSegundoNumero = new JLabel("Segundo numero");
		lblSegundoNumero.setBounds(41, 88, 86, 14);
		frame.getContentPane().add(lblSegundoNumero);
		
		lblTercerNumero = new JLabel("Tercer numero");
		lblTercerNumero.setBounds(41, 154, 86, 14);
		frame.getContentPane().add(lblTercerNumero);
		
		lblElMayorNumero = new JLabel("El mayor numero es:");
		lblElMayorNumero.setBounds(300, 99, 151, 14);
		frame.getContentPane().add(lblElMayorNumero);
	}

}
