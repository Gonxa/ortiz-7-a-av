package ejerciciosConPantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Ejer1 {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JLabel lblTrim_1;
	private JLabel lblTrim_2;
	private JTextField textField_2;
	private JLabel lblfinal;
	private JButton btnNewButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejer1 window = new Ejer1();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejer1() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 492, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(45, 55, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(189, 55, 86, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblTrim = new JLabel("1\u00BA TRIM");
		lblTrim.setBounds(64, 30, 46, 14);
		frame.getContentPane().add(lblTrim);
		
		lblTrim_1 = new JLabel("2\u00BA TRIM");
		lblTrim_1.setBounds(211, 30, 46, 14);
		frame.getContentPane().add(lblTrim_1);
		
		lblTrim_2 = new JLabel("3\u00BA TRIM");
		lblTrim_2.setBounds(355, 30, 46, 14);
		frame.getContentPane().add(lblTrim_2);
		
		textField_2 = new JTextField();
		textField_2.setBounds(334, 55, 86, 20);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		lblfinal = new JLabel("");
		lblfinal.setBounds(157, 178, 169, 43);
		frame.getContentPane().add(lblfinal);
		
		btnNewButton = new JButton("");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				float prom= (Integer.parseInt(textField.getText())+Integer.parseInt(textField_1.getText())+Integer.parseInt(textField_2.getText()))/3;
				if (prom>=7){
					lblfinal.setText("Aprobado");
				}else{
					lblfinal.setText("Desaprobado");
				}
				
			}
		});
		btnNewButton.setBounds(189, 106, 89, 23);
		frame.getContentPane().add(btnNewButton);
	}

}
