package ejerciciosConPantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Ejer6 {

	private JFrame frame;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejer6 window = new Ejer6();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejer6() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		final JLabel label = new JLabel("");
		label.setBounds(119, 173, 185, 39);
		frame.getContentPane().add(label);
		
		textField = new JTextField();
		textField.setBounds(60, 69, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton button = new JButton("");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int grado = Integer.parseInt(textField.getText());
				if (grado==0){
					label.setText("jard�n de infantes");
				}else if (grado>=1 && grado<=6){
					label.setText("primaria");
				}else if (grado>=7 && grado<=12){
					label.setText("secundaria");
				}else{
					label.setText("Fuera de limite");
				}
			}
		});
		button.setBounds(264, 68, 89, 23);
		frame.getContentPane().add(button);
		
		JLabel lblIngrese = new JLabel("Ingrese curso");
		lblIngrese.setBounds(63, 41, 108, 14);
		frame.getContentPane().add(lblIngrese);
	}

}
