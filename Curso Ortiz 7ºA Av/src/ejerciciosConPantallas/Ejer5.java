package ejerciciosConPantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Ejer5 {

	private JFrame frame;
	private JTextField textField;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejer5 window = new Ejer5();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejer5() {
		initialize();
		///new JLabel("");
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		final JLabel label = new JLabel("");
		label.setBounds(27, 165, 382, 34);
		frame.getContentPane().add(label);
		
		textField = new JTextField();
		textField.setBounds(75, 62, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton button = new JButton("");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int puesto = Integer.parseInt(textField.getText());
				if (puesto==1){
					label.setText("El primero obtiene la medalla de oro.");
				}else if (puesto==2){
					label.setText("El segundo obtiene la medalla de plata.");
				}else if (puesto==3){
					label.setText("El tercero obtiene la medalla de bronce.");
				}else{
					label.setText("Siga participando.");
				}
				
			}
		});
		button.setBounds(269, 61, 89, 23);
		frame.getContentPane().add(button);
		
		JLabel lblNumero = new JLabel("Posicion");
		lblNumero.setBounds(82, 37, 89, 14);
		frame.getContentPane().add(lblNumero);
		
		JLabel lblPosicion = new JLabel("Medalla");
		lblPosicion.setBounds(82, 147, 89, 14);
		frame.getContentPane().add(lblPosicion);
	}

}
