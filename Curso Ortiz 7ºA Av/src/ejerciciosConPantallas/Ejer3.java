package ejerciciosConPantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font;

public class Ejer3 {

	private JFrame frame;
	private JTextField textMes;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejer3 window = new Ejer3();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejer3() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textMes = new JTextField();
		textMes.setBounds(40, 115, 86, 20);
		frame.getContentPane().add(textMes);
		textMes.setColumns(10);
		
		
		final JLabel label = new JLabel("");
		label.setBounds(278, 101, 109, 43);
		frame.getContentPane().add(label);
		
		JButton btnButton = new JButton("");
		btnButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String mes= textMes.getText();
				if (mes.equals("noviembre") | mes.equals("abril") | mes.equals("junio") | mes.equals("septiembre")){
					label.setText("30");
				}else if (mes.equals("febrero")){
					label.setText("28");
				}else{
					label.setText("31");
				}
			}
		});
		btnButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnButton.setBounds(154, 114, 89, 23);
		frame.getContentPane().add(btnButton);
		
		JLabel lblIngreseElMes = new JLabel("Ingrese el mes");
		lblIngreseElMes.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblIngreseElMes.setBounds(40, 80, 145, 20);
		frame.getContentPane().add(lblIngreseElMes);
		
		JLabel lblDias = new JLabel("D\u00EDas:");
		lblDias.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblDias.setBounds(261, 85, 145, 20);
		frame.getContentPane().add(lblDias);
	}

}
