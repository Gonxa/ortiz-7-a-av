package ejerciciosConPantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Ejer2 {

	private JFrame frame;
	private JTextField textField;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejer2 window = new Ejer2();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejer2() {
		initialize();
		new JLabel("");
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(39, 116, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		final JLabel label = new JLabel("");
		label.setBounds(307, 116, 58, 37);
		frame.getContentPane().add(label);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int num=Integer.parseInt(textField.getText());
				if (num%2==0){
					label.setText("Par");
				}else{
					label.setText("Impar");
				}
				
			}
		});
		btnNewButton.setBounds(156, 115, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblIngreseNumero = new JLabel("Ingrese numero");
		lblIngreseNumero.setBounds(39, 91, 106, 14);
		frame.getContentPane().add(lblIngreseNumero);
		
		JLabel lblElNumeroEs = new JLabel("El numero es:");
		lblElNumeroEs.setBounds(276, 91, 106, 14);
		frame.getContentPane().add(lblElNumeroEs);
		
	}
}
