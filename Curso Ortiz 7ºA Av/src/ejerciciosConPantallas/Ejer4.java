package ejerciciosConPantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Ejer4 {

	private JFrame frame;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejer4 window = new Ejer4();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejer4() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(41, 123, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		final JLabel label = new JLabel("");
		label.setBounds(286, 110, 101, 33);
		frame.getContentPane().add(label);
		
		JButton button = new JButton("");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				char letra= textField.getText().charAt(0);
				if (letra=='a'){
					label.setText("hijo");
				}else if (letra=='b'){
					label.setText("padre");
				}else if (letra=='c'){
					label.setText("abuelo");
				}
			}
		});
		button.setBounds(162, 122, 89, 23);
		frame.getContentPane().add(button);
		
		JLabel lblIngreseLetra = new JLabel("Ingrese letra");
		lblIngreseLetra.setBounds(41, 91, 101, 14);
		frame.getContentPane().add(lblIngreseLetra);
	}

}
